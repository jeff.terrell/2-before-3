(ns edu.unc.cs.course
  (:require [clojure.string :as str]
            [edu.unc.cs.schedule :refer [parse-schedule schedules-conflict?]]))

(defrecord Course
    [id raw-number number instructor schedule cap block-id assignments remote? course-id])

(defn new-course [[name id instructor schedule-str cap block-id modality course-id]]
  (let [number (subs name 0 3)]
    (->Course (Integer/parseInt id)
              (first (str/split name #"\s+"))
              number
              instructor
              (parse-schedule schedule-str)
              (int (Float/parseFloat cap))
              block-id
              []
              (boolean (re-find #"(?i)Remote" modality))
              (Integer/parseInt course-id))))

(defn new-course-fall21
  ;; the fields changed a bit in the fall 2021 course list
  [[name _ id _ _ _ instructor schedule-str block-id cap modality _]]
  (new-course [name id instructor schedule-str cap block-id modality]))

(defn new-course-spring22
  [[name _ id _ _ _ instructor schedule-str block-id cap modality course-id]]
  (new-course [name id instructor schedule-str cap block-id modality course-id]))

(defn courses-conflict? [c1 c2]
  (or (= (:number c1) (:number c2))
      (schedules-conflict? (:schedule c1) (:schedule c2))))

(defn make-courses-conflict?-fn [courses]
  (let [n (count courses)
        index-pairs (for [i (range n)
                          j (range (inc i))]
                      [i j])
        make-key (fn [id1 id2] (if (<= id1 id2)
                                 [id1 id2]
                                 [id2 id1]))
        key-value-pairs (for [[i j] index-pairs]
                          (let [c1 (get courses i)
                                c2 (get courses j)
                                key (make-key (:id c1) (:id c2))
                                val (courses-conflict? c1 c2)]
                            [key val]))
        memoization-table (into {} key-value-pairs)]
    (fn [id1 id2]
      (get memoization-table (make-key id1 id2)))))
