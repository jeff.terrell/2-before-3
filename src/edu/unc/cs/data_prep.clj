(ns edu.unc.cs.data-prep
  (:require [edu.unc.cs.course :refer [make-courses-conflict?-fn
                                       new-course-spring22]]
            [edu.unc.cs.student :refer [new-student]]
            [edu.unc.cs.util :refer [csv-path->vecs index]]
            [edu.unc.cs.validate
             :refer [validate-courses-needed
                     validate-remote-courses
                     validate-semesters-left
                     validate-student-picks!
                     validate-student-pid-onyen-consistency]]))

(defn read-course-data [course-csv-path]
  (csv-path->vecs course-csv-path
                  ["Name" "Count (Requests)" "ID" "Course" "Term" "Section"
                   "Instructor(s)" "Schedule" "Block ID" "Enroll Cap" "Modality"
                   "CourseID"]))

(defn read-student-data [student-csv-path]
  (try
    (csv-path->vecs student-csv-path
                    ["PID" "First Name" "Last Name" "ONYEN" "Course Selections"
                     "Email" "Semesters Remaining" "Courses Needed"
                     "Computer Science Degree Program" "Created By" "On Campus"])
    (catch AssertionError _
      ;; Jack's input is a little different. But we can handle that input the
      ;; exact same way because fields are treated positionally instead of by
      ;; name.
      (csv-path->vecs student-csv-path
                      ["ID" "First Name" "Last Name" "ONYEN" "Course Selections"
                       "Email" "Semesters Remaining" "Courses Needed"
                       "Computer Science Degree Program" "Created By"
                       "On Campus"]))))

(defn read-data [course-csv-path student-csv-path]
  [(read-course-data course-csv-path)
   (read-student-data student-csv-path)])

(defn clamp-courses-needed [students]
  (vec (for [{:keys [courses-needed] :as student} students]
         (-> student
             (assoc :submitted-courses-needed courses-needed)
             (update :courses-needed min 2)))))

(defn override-earlier-submissions [students]
  (loop [pids-seen (transient #{})
         onyens-seen (transient #{})
         latest-submissions (list)
         students (reverse students)]
    (if-not (seq students)
      latest-submissions
      (let [[student & students] students
            {:keys [pid onyen] :as submission} student]
        (if (or (contains? pids-seen pid)
                (contains? onyens-seen onyen))
          (recur pids-seen onyens-seen latest-submissions students)
          (recur (conj! pids-seen pid)
                 (conj! onyens-seen onyen)
                 (cons submission latest-submissions)
                 students))))))

(defn separate-cohort [cohort-key students]
  (when (seq students)
    (let [groups (group-by cohort-key students)
          min-semesters (apply min (keys groups))
          cohort (index :onyen (get groups min-semesters))
          others (->> (dissoc groups min-semesters)
                      vals
                      (apply concat))]
      [cohort others])))

(defn initialize-data [config raw-course-vecs raw-student-vecs]
  (let [{:keys [cohort-key give-complete-students-1-course]} config
        courses (mapv new-course-spring22 raw-course-vecs)
        students-with-dupes (mapv new-student raw-student-vecs)
        students (override-earlier-submissions students-with-dupes)
        _ (validate-student-picks! (set (map :id courses)) students)
        set-courses-needed-0->1 #(if (zero? (:courses-needed %))
                                   (assoc % :courses-needed 1)
                                   %)
        courses-needed->=1? #(< 0 (:courses-needed %))
        handle-courses-needed (if give-complete-students-1-course
                                #(map set-courses-needed-0->1 %)
                                #(filter courses-needed->=1? %))
        students (->> students
                      (filter #(< 0 (cohort-key %) 11))
                      clamp-courses-needed
                      handle-courses-needed)
        [remote on-campus] (map (fn [remote?] (filter #(= remote? (:remote? %))
                                                      students))
                                [true false])
        [remote remote-warnings] (validate-remote-courses remote courses)
        warnings (str
                   (validate-student-pid-onyen-consistency students-with-dupes)
                   (validate-semesters-left students)
                   (when-not give-complete-students-1-course
                     (validate-courses-needed students))
                   remote-warnings)]

    (when (not-empty warnings)
      (binding [*out* *err*]
        (print warnings)
        (flush)))

    {:courses (index :id courses)
     :courses-conflict? (make-courses-conflict?-fn courses)
     :students students
     :remote remote
     :on-campus on-campus
     :warnings warnings}))
