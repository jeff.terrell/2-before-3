(ns edu.unc.cs.two-before-three
  (:gen-class)
  (:require [clojure.java.io :as io]
            [clojure.tools.cli :as cli]
            [edu.unc.cs.assign :refer [assign-all]]
            [edu.unc.cs.cli :refer [validate-cli-args!]]
            [edu.unc.cs.data-prep :refer [initialize-data read-data]]
            [edu.unc.cs.output :refer [blockreg-csvs cohort-results
                                       cohort-results->tsv course-stats
                                       courses->csv human-tsvs student-results
                                       student-results->tsv students->csv
                                       cover-sheet-csv]]
            [edu.unc.cs.util :refer [index my-shuffle spit-multiple split-dir-base]]
            [edu.unc.cs.validate :refer [validate-output-noclobber!]]))

(def VERSION "0.1.0-alpha")
(defn cohort-key
  "How to group students into cohorts. A cohort is defined as all the students
  having the same return value from this function. The obvious cohort key is
  simply `:semesters-left`."
  [{:keys [remote? semesters-left]}]
  (if remote? 0 semesters-left))

(def give-complete-students-1-course
  "Should students who have completed all degree requirements still go through
  the algorithm to get a max of 1 courses from their picklist?"
  false)

(defn functional-core [config [raw-courses raw-students]]
  (let [{:keys [cohort-key output-base]} config
        init-data (initialize-data config raw-courses raw-students)
        {:keys [remote on-campus students]} init-data
        phase1-data (assoc init-data :students remote)
        phase2-courses (assign-all cohort-key phase1-data)
        phase2-data (-> init-data
                        (assoc :courses (index :id phase2-courses))
                        (assoc :students on-campus))
        final-courses (assign-all cohort-key phase2-data)
        student-results (student-results final-courses students)
        blockreg-csvs (blockreg-csvs output-base students final-courses)
        human-tsvs (human-tsvs output-base student-results final-courses)
        out-path #(str output-base %)]

    (merge
      blockreg-csvs
      human-tsvs
      {(out-path "warnings.txt") (:warnings init-data)
       (out-path "student-results.tsv") (student-results->tsv student-results)
       (out-path "cohort-results.tsv") (->> student-results
                                            (sort-by cohort-key)
                                            (cohort-results cohort-key)
                                            cohort-results->tsv)
       (out-path "course-stats.tsv") (course-stats cohort-key final-courses
                                                   students)
       (out-path "courses-out.csv") (courses->csv final-courses)
       (out-path "requests-out.csv") (students->csv final-courses init-data)
       (out-path "blockreg.cover-sheet.csv") (cover-sheet-csv final-courses)})))

(defn -main [& args]
  (let [{:keys [output-base random-seed courses-csv
                requests-csv] :as config} (validate-cli-args! args)
        config (-> config
                   (assoc :cohort-key cohort-key)
                   (assoc :give-complete-students-1-course
                          give-complete-students-1-course))
        [output-dir output-file-base] (split-dir-base output-base)]
    (validate-output-noclobber! output-dir output-file-base)
    (let [raw-data (read-data courses-csv requests-csv)
          random (if random-seed
                   (new java.util.Random random-seed)
                   (new java.util.Random))
          csvs-to-write (with-redefs [shuffle (partial my-shuffle random)]
                          (functional-core config raw-data))]
      (when-not (-> output-dir io/file .isDirectory)
        (println "Creating directory" output-dir)
        (io/make-parents (-> csvs-to-write first key)))
      (spit-multiple csvs-to-write)
      (println "Success!")
      (println (str "- Warning output above, if any, is copied to "
                    output-base "warnings.txt"))
      (println (str "- Block registration cover sheet is at " output-base
                    "blockreg.cover-sheet.csv"))
      (println (str "- Block registration CSVs are at " output-base
                    "blockreg.{course}.{block-id}.{instructor}.csv"))
      (println (str "- Human readable TSVs with the same info are at "
                    output-base
                    "human.{course}.{block-id}.{instructor}.tsv"))
      (println (str "- Data about student results is at " output-base
                    "student-results.tsv"))
      (println (str "- Analysis about cohorts is at " output-base
                    "cohort-results.tsv"))
      (println (str "- Analysis about courses is at " output-base
                    "course-stats.tsv"))
      (println (str "- Final course data is at " output-base
                    "courses-out.csv"))
      (println (str "- Final student request data is at " output-base
                    "requests-out.csv")))))
