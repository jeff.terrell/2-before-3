(ns edu.unc.cs.util
  (:require [clojure.data.csv :as csv]
            [clojure.java.io :as io]
            [clojure.string :as str])
  (:import (java.io StringWriter)))

(defn csv-path->vecs
  "Given a path to an input file, open it and read it as a CSV file. Return a
  sequence of vectors of strings, where each vector contains the fields of the
  corresponding line."
  [path headers]
  (with-open [reader (io/reader path)]
    (.skip reader 1) ;; skip byte order mark (BOM)
    (let [rows (csv/read-csv reader)]
      (assert (= headers (first rows))
              (format "While reading %s, expected headers to be %s but they were %s"
                      path (pr-str headers) (pr-str (first rows))))
      (-> rows rest doall))))

(defn index [keyfn items]
  (when (seq items)
    (into {}
          (for [item items]
            [(keyfn item) item]))))

(defn table->csv [table]
  (with-out-str (csv/write-csv *out* table)))

(defn table->tsv [table]
  (letfn [(to-str [field]
            (if (sequential? field)
              (format "\"%s\"" (str/join "," field))
              (str field)))
          (row->line [fields]
            (str (str/join "\t" (map to-str fields)) "\n"))]
    (->> table
         (map row->line)
         (apply str))))

(defn spit-multiple [filename-contents-map]
  (doseq [[filename contents] filename-contents-map]
    (spit filename contents)))

(defn csv-string [rows]
  (let [string-writer (new StringWriter)]
    (.append string-writer (char 0xffef)) ; append byte order mark
    (csv/write-csv string-writer rows)
    (str string-writer)))

;; modified from clojure.core/shuffle
(defn my-shuffle [^java.util.Random random ^java.util.Collection coll]
  (let [al (java.util.ArrayList. coll)]
    (java.util.Collections/shuffle al random)
    (clojure.lang.RT/vector (.toArray al))))

(defn die!
  ([status message] (die! status message true))
  ([status message stderr?]
   (binding [*out* (if stderr? *err* *out*)]
     (println message)
     (System/exit status))))

(defn split-dir-base [base]
  (let [base-file (io/file base)
        file-part (.getName base-file)]
    (if (or (.endsWith base "/") (.isDirectory base-file))
      [(.getPath base-file) ""]
      (if-let [parent-dir (.getParent base-file)]
        [parent-dir file-part]
        ["." file-part]))))
