(ns edu.unc.cs.interval
  "An interval is a hashmap with a begin and end value. Each value is a
  java.time.LocalTime instance."
  (:require [clojure.string :as str])
  (:import (java.time LocalTime)
           (java.time.format DateTimeFormatter)))

(defn parse-interval
  "Parse an interval like 3:30pm-4:45pm into an interval hashmap."
  [interval-str]
  (let [[start end] (str/split interval-str #"-")
        long-formatter (DateTimeFormatter/ofPattern "h:mma")
        short-formatter (DateTimeFormatter/ofPattern "ha")
        parse-long-time #(LocalTime/parse (.toUpperCase %) long-formatter)
        parse-short-time #(LocalTime/parse (.toUpperCase %) short-formatter)
        parse-time #(if (= -1 (.indexOf % ":"))
                     (parse-short-time %)
                     (parse-long-time %))]
    {:begin (parse-time start)
     :end (parse-time end)}))

(defn intervals-conflict? [t1 t2]
  ;; See https://codereview.stackexchange.com/a/206719 for explanation of logic.
  (and
    (.isBefore (:begin t1) (:end t2))
    (.isBefore (:begin t2) (:end t1))))
