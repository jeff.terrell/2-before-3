(ns edu.unc.cs.schedule
  "A Schedule contains a sequence of intervals for each weekday."
  (:require [clojure.string :as str]
            [edu.unc.cs.interval :refer [parse-interval intervals-conflict?]]))

(defrecord Schedule [input-str mon tue wed thu fri])

(defn parse-schedule [schedule-str]
  (let [abbreviations {"TTh" [0 1 0 1 0]
                       "W" [0 0 1 0 0]
                       "MW" [1 0 1 0 0]
                       "MWF" [1 0 1 0 1]
                       "Mo" [1 0 0 0 0]
                       "Tu" [0 1 0 0 0]
                       "We" [0 0 1 0 0]
                       "Th" [0 0 0 1 0]
                       "Fr" [0 0 0 0 1]}
        [weekdays interval-str] (str/split schedule-str #" ")
        interval (parse-interval interval-str)
        err (str "Error: I need to know which weekdays the string '%s' "
                 "represents, but I don't have that information available. "
                 "(The full schedule string was '%s'.)")]
    (assert (contains? abbreviations weekdays)
            (format err weekdays schedule-str))
    (apply ->Schedule schedule-str
           (for [n (get abbreviations weekdays)]
             (repeat n interval)))))

(defn schedules-conflict? [s1 s2]
  (boolean
    (some identity
          (for [day [:mon :tue :wed :thu :fri]
                interval1 (get s1 day)
                interval2 (get s2 day)]
            (intervals-conflict? interval1 interval2)))))
