(ns edu.unc.cs.student
  (:require [clojure.string :as str]))

(defrecord Student [pid onyen picks semesters-left courses-needed remote?])

(defn new-student
  [[pid _first _last onyen picks-str _email semesters-left courses-needed
    remote-status]]
  (letfn [(parse-int [string] (Integer/parseInt string))
          (strip-at [string] (str/replace string #"@.*$" ""))]
    (assert (re-matches #"\d+(, \d+)*" picks-str)
            (format "Invalid picks string (%s) for onyen '%s'" picks-str onyen))
    (->Student pid
               (-> onyen .toLowerCase str/trim strip-at)
               (mapv parse-int (str/split picks-str #", "))
               (parse-int semesters-left)
               (parse-int courses-needed)
               (= remote-status "Remote"))))
