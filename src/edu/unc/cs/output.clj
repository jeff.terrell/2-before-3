(ns edu.unc.cs.output
  (:require [clojure.string :as str]
            [edu.unc.cs.assign :refer [assign]]
            [edu.unc.cs.util :refer [csv-string index table->csv table->tsv]]))

(def unsatisfied-desc-onyen-asc
  (juxt (comp #(- 2 %) :unsatisfied-count) :onyen))

(defn student-results [courses students]
  (let [students (mapv (fn [{:keys [courses-needed] :as student}]
                         (assoc student :unsatisfied-count courses-needed))
                       students)
        by-onyen (index :onyen students)
        assignment-pairs (for [{:keys [id assignments]} courses
                               onyen assignments]
                           [id onyen])

        conj* (fnil conj [])
        add-assignment (fn [student course-id]
                         (let [{:keys [courses-needed picks]} student
                               rank (inc (.indexOf picks course-id))]
                           (-> student
                               (update :unsatisfied-count dec)
                               (update :assignments conj* course-id))))

        sort-assignments (fn [{:keys [picks assignments] :as student}]
                           (let [assigned? (set assignments)
                                 sorted (filterv assigned? picks)
                                 ranks (vec (for [course-id sorted]
                                              (inc (.indexOf picks course-id))))]
                             (-> student
                                 (assoc :assignments sorted)
                                 (assoc :assignment-ranks ranks))))]

    (->> (reduce (fn [by-onyen [course-id onyen]]
                   (update by-onyen onyen add-assignment course-id))
                 by-onyen
                 assignment-pairs)
         vals
         (map sort-assignments)
         (sort-by unsatisfied-desc-onyen-asc))))

(defn cohort-results [cohort-key student-results]
  (letfn [(sum [numbers] (reduce + numbers))
          (single-seats-needed [student]
            (min (:courses-needed student)
                 (-> student :picks count)))
          (seats-needed [students]
            (->> students (map single-seats-needed) sum))
          (seats-assigned [students]
            (->> students (map (comp count :assignments)) sum))
          (got-first-choice? [student]
            (= 1 (-> student :assignment-ranks first)))
          (first-choices [students]
            (count (filter got-first-choice? students)))
          (avg [numbers]
            (let [n (count numbers)]
              (when-not (zero? n)
                (float (/ (sum numbers) (count numbers))))))
          (avg-1st [students]
            (->> students (keep (comp first :assignment-ranks)) avg))
          (avg-2nd [students]
            (->> students (keep (comp second :assignment-ranks)) avg))]

    (vec
      (for [students (partition-by cohort-key student-results)]
        {:cohort (-> students first cohort-key)
         :n (count students)
         :seats-needed (seats-needed students)
         :seats-assigned (seats-assigned students)
         :first-choices (first-choices students)
         :avg-first-rank (avg-1st students)
         :avg-second-rank (avg-2nd students)
         :avg-picklist-length (avg (map (comp count :picks) students))}))))

(defn student-results->tsv [student-results]
  (table->tsv
    (cons
      ["PID" "onyen" "semesters-left" "courses-needed" "picks" "assignments"
       "assignment-ranks" "unsatisfied-count"]
      (map (juxt :pid :onyen :semesters-left :submitted-courses-needed :picks
                 :assignments :assignment-ranks :unsatisfied-count)
           student-results))))

(defn cohort-results->tsv [cohort-results]
  (table->tsv
    (cons
      ["cohort" "N" "seats-needed" "seats-assigned" "first-choices"
       "avg-first-rank" "avg-second-rank" "avg-picklist-length"]
      (map (juxt :cohort :n :seats-needed :seats-assigned :first-choices
                 :avg-first-rank :avg-second-rank :avg-picklist-length)
           cohort-results))))

(defn course-stats [cohort-key final-courses students]
  (table->tsv
    (cons ["number.section" "instructor" "remote?" "cap" "num-picks" "avg-pick-rank"
           "num-assigned" "remaining-space" "assigned-avg-cohort"]
          (map (fn [{:keys [id raw-number instructor cap assignments remote?]}]
                 (let [n (count assignments)
                       picked-this? #(.contains (:picks %) id)
                       pickers (filter picked-this? students)
                       ranks (map #(inc (.indexOf (:picks %) id)) pickers)
                       avg #(when (not-empty %)
                              (float (/ (reduce + %)
                                        (count %))))
                       assigned? (comp (set assignments) :onyen)
                       assigned (filter assigned? students)]
                   [raw-number
                    instructor
                    (if remote? "yes" "no")
                    cap
                    (count pickers)
                    (avg ranks)
                    n
                    (- cap n)
                    (avg (map cohort-key assigned))]))
               (sort-by :number final-courses)))))

(defn course->blockreg-table [students-by-onyen course]
  (let [onyen->pid #(get-in students-by-onyen [% :pid])]
    (mapv (juxt onyen->pid (constantly "UGRD")) (:assignments course))))

(defn course->human-table [students-by-onyen course]
  (letfn [(onyen->row [onyen]
            (let [student (get students-by-onyen onyen)
                  fields [:pid :onyen :semesters-left :submitted-courses-needed
                          :picks :assignments :assignment-ranks
                          :unsatisfied-count]]
              (mapv (partial get student) fields)))]
    (->> (:assignments course)
         (sort-by (comp unsatisfied-desc-onyen-asc students-by-onyen))
         (mapv onyen->row))))

(defn blockreg-csvs [output-base students courses]
  (let [by-onyen (index :onyen students)
        course->csv-filename (fn [{:keys [raw-number instructor block-id]}]
                               (format "%sblockreg.comp%s.%s.%s.csv"
                                       output-base raw-number block-id
                                       (.toLowerCase instructor)))]
    (into {}
          (for [course courses]
            [(course->csv-filename course)
             (table->csv (course->blockreg-table by-onyen course))]))))

(defn human-tsvs [output-base students courses]
  (let [by-onyen (index :onyen students)
        course->tsv-filename (fn [{:keys [raw-number instructor block-id]}]
                               (format "%shuman.comp%s.%s.%s.tsv"
                                       output-base raw-number block-id
                                       (.toLowerCase instructor)))]
    (into {}
          (for [course courses]
            [(course->tsv-filename course)
             (table->tsv
               (cons
                 ["PID" "onyen" "semesters-left" "courses-needed" "picks"
                  "assignments" "assignment-ranks" "unsatisfied-count"]
                 (course->human-table by-onyen course)))]))))

(defn courses->csv [courses]
  (letfn [(course->row [{:keys [id raw-number instructor schedule cap block-id
                                assignments]}]
            [(str raw-number " - <snip>")
             id
             instructor
             (:input-str schedule)
             (str (- cap (count assignments)) ".0")
             block-id])]
    (csv-string
      (cons ["Name" "ID" "Instructor(s)" "Schedule" "Enroll Cap" "Block ID"]
            (->> courses
                 (sort-by :raw-number)
                 (map course->row))))))

(defn cover-sheet-csv [courses]
  (letfn [(course->row [{:keys [id raw-number block-id cap assignments
                                course-id]}]
            (let [[num sec] (str/split raw-number #"\.")]
              [(format "COMP %s-%s" num sec)
               (str "CS" num)
               (str "CS" num)
               (format "CS%s.%s" num block-id)
               (count assignments)
               course-id
               "Y"
               "Y"
               "N/A"
               cap]))]
    (csv-string
      (cons ["COMP COURSE" "Student Block" "Class Block" "Class Description"
             "Total Students" "Course Number" "Requistes Override"
             "Instructor Permission" "RECITATION Section Number"
             "Total Course Enrollment Capacity"]
            (->> courses
                 (sort-by :raw-number)
                 (map course->row))))))

(defn students->csv [final-courses init-data]
  ;; We could have conveyed the updated students along with the updated courses,
  ;; but it wasn't needed initially, and now it would be a fairly thoroughgoing
  ;; change. So instead, let's update the original students sequence based on
  ;; the assignments noted in the final courses. Then we can serialize the
  ;; students.

  (let [init-students (:students init-data)
        data (assoc init-data :current-cohort (index :onyen init-students))
        assignment-pairs (for [{:keys [id assignments]} final-courses
                               onyen assignments]
                           [id onyen])
        accum-assignment (fn [data [course-id onyen]]
                           (assign data course-id onyen))
        updated-data (reduce accum-assignment data assignment-pairs)
        header-row ["PID" "First Name" "Last Name" "ONYEN" "Course Selections"
                    "Email" "Semesters Remaining" "Courses Needed"
                    "Computer Science Degree Program" "Created By"]

        student->row (fn [{:keys [pid onyen picks semesters-left
                                  courses-needed]}]
                       [pid
                        "" ; first
                        "" ; last
                        onyen
                        (str/join ", " picks)
                        "" ; email
                        semesters-left
                        courses-needed
                        "" ; CS degree program
                        "" ; created by
                        ])]

    (csv-string
      (cons header-row
            (->> (-> updated-data :current-cohort vals)
                 (filter (comp pos? :courses-needed))
                 (filter (comp not-empty :picks))
                 (sort-by :onyen)
                 (map student->row))))))
