(ns edu.unc.cs.cli
  (:require [clojure.java.io :as io]
            [clojure.string :as str]
            [clojure.tools.cli :as cli]
            [edu.unc.cs.util :refer [die!]]))

(def cli-config
  (let [is-file? #(let [f (io/file %)] (and (.exists f) (.canRead f)))
        parse-int #(Integer/parseInt %)]
    [["-h" "--help"]
     ["-c" "--courses-csv FILE" "input CSV file for course data"
      :required "FILE"
      :validate [is-file? "Must be readable"]]
     ["-o" "--output-base PATH_BASE" "output base (dirs created automatically)"
      :default "output/"]
     ["-r" "--requests-csv FILE" "input CSV file for student request data"
      :required "FILE"
      :validate [is-file? "Must be readable"]]
     ["-s" "--random-seed NUMBER" "the seed for the PNRG (default: none)"
      :default nil
      :parse-fn parse-int
      :validate [#(< 0 % 0x10000) "Must be a number between 0 and 65536"]]]))

(def synopsis
  (format "two-before-three: %s\n\nUsage:\n    %s\n    %s\n\n"
          "compute preregistration assignments for UNC CS students"
          "clojure -M -m edu.unc.cs.two-before-three -c FILE -r FILE [opts...]"
          "java -jar edu.unc.cs.two-before-three.jar -c FILE -r FILE [opts...]"))

(defn validate-cli-args! [args]
  (let [opts (cli/parse-opts args cli-config)]
    (when (:help (:options opts))
      (die! 0 (str synopsis (:summary opts))))
    (let [{:keys [arguments errors options]} opts
          err-str #(format
                     "You must specify a CSV file with %s data (see %s option)"
                     %1 %2)
          conj-err-str #(conj %1 (err-str %2 %3))
          no-args (format "No extra arguments are accepted, but you gave: '%s'"
                          (str/join " " arguments))
          errors (cond-> (or errors [])
                   (-> options :courses-csv not) (conj-err-str "courses" "-c")
                   (-> options :requests-csv not) (conj-err-str
                                                    "student request" "-r")
                   (pos? (count arguments)) (conj no-args))]
      (when (pos? (count errors))
        (die! 1
             (if (= 1 (count errors))
               (first errors)
               (str "There were some errors:\n\n- "
                    (str/join "\n- " errors)))))
      options)))
