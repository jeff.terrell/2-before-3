(ns edu.unc.cs.assign
  (:require [edu.unc.cs.data-prep :refer [separate-cohort]]))

(defn assign [data course-id onyen]
  (letfn [(add-student-to-assignments [course]
            (update course :assignments conj onyen))
          (filter-picks [picks]
            (filterv (complement (partial (:courses-conflict? data) course-id))
                     picks))
          (update-picks [student]
            (update student :picks filter-picks))]
    (-> data
        (update-in [:courses course-id] add-student-to-assignments)
        (update-in [:current-cohort onyen :courses-needed] dec)
        (update-in [:current-cohort onyen] update-picks))))

(defn assign-multiple [data course-id onyens]
  (let [{:keys [assignments cap]} (get-in data [:courses course-id])
        space (- cap (count assignments))
        onyens-to-assign (if (<= (count onyens) space)
                           onyens
                           (->> onyens shuffle (take space)))]
    (reduce #(assign %1 course-id %2) data onyens-to-assign)))

(defn sweep-course [data course-id]
  (letfn [(sweep-vector [v] (filter #(not= course-id %) v))
          (sweep-student [s] (update s :picks sweep-vector))
          (sweep-students [ss] (mapv sweep-student ss))
          (sweep-cohort [c]
            (into {}
                  (for [[onyen student] c]
                    [onyen (sweep-student student)])))]
    (-> data
        (update :courses dissoc course-id)
        (update :full-courses (fnil conj []) (get-in data [:courses course-id]))
        (update :students sweep-students)
        (update :current-cohort sweep-cohort))))

(defn assign-multiple-and-sweep-if-needed [data course-id onyens]
  (let [data (assign-multiple data course-id onyens)
        {:keys [assignments cap]} (get-in data [:courses course-id])
        space (- cap (count assignments))]
    (if (zero? space)
      (sweep-course data course-id)
      data)))

(defn assign-top-picks [data]
  (let [students-by-top-pick (group-by (comp first :picks)
                                       (-> data :current-cohort vals))]
    (reduce (fn [data [course-id students]]
              (assign-multiple-and-sweep-if-needed data course-id
                                                   (map :onyen students)))
            data students-by-top-pick)))

(defn sweep-students [data]
  (letfn [(keep-student? [{:keys [courses-needed picks]}]
            (and (< 0 courses-needed)
                 (seq picks)))
          (filter-vals [predicate hashmap]
            (into (empty hashmap)
                  (for [[k v] hashmap
                        :when (predicate v)]
                    [k v])))
          (sweep-index [students-by-id]
            (filter-vals keep-student? students-by-id))
          (sweep-sequence [students]
            (filterv keep-student? students))]
    (-> data
        (update :current-cohort sweep-index)
        (update :students sweep-sequence))))

(defn assign-current-cohort [init-data]
  (let [course-full? (fn [{:keys [assignments cap]}]
                       (= cap (count assignments)))]
    (loop [data init-data]
      (if (or (every? course-full? (-> data :courses vals))
              (empty? (:current-cohort data)))
        data
        (recur (-> data assign-top-picks sweep-students))))))

(defn assign-all [cohort-key init-data]
  (loop [data init-data]
    (if-let [[cohort remaining-students] (separate-cohort cohort-key
                                                          (:students data))]
      (let [data (-> data
                     (assoc :current-cohort cohort)
                     (assoc :students remaining-students))]
        (recur (assign-current-cohort data)))
      (let [{:keys [full-courses courses]} data]
        (apply conj (or full-courses []) (vals courses))))))
