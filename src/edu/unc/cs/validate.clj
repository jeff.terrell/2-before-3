(ns edu.unc.cs.validate
  (:require [clojure.java.io :as io]
            [clojure.string :as str]
            [edu.unc.cs.util :refer [die!]])
  (:import (java.nio.file FileSystems)))

(defn validate-student-pid-onyen-consistency [students]
  (let [pairs (mapv (juxt :pid :onyen) students)
        pid-groups (->> pairs (group-by first) vals)
        onyen-groups (->> pairs (group-by second) vals)
        make-dupe-group (fn [keyfn valfn groups]
                          (let [get-key #(-> % first keyfn)
                                get-val #(mapv valfn %)
                                group->dupe (juxt get-key get-val)]
                            (->> groups
                                 (filter #(< 1 (count %)))
                                 (mapv group->dupe))))

        p-dupe-groups (make-dupe-group first second pid-groups)
        o-dupe-groups (make-dupe-group second first onyen-groups)
        p-conflicts (filterv (fn [[onyen pids]] (< 1 (count (set pids))))
                             o-dupe-groups)
        o-conflicts (filterv (fn [[pid onyens]] (< 1 (count (set onyens))))
                             p-dupe-groups)

        conflict-line (fn [field1 field2 [val1 val2]]
                        (format "- %s: '%s'; %s: %s (using last value %s)\n"
                                field1
                                val1
                                field2
                                (str/join ", " val2)
                                (last val2)))

        err-str (fn [field1 field2 conflicts]
                  (if (empty? conflicts)
                    ""
                    (format
                      "WARNING: different %s were seen for the same %s in the following cases:\n%s\n"
                      field2 field1
                      (apply str
                             (map (partial conflict-line field1 field2)
                                  conflicts)))))]

    (str (err-str "onyen" "pids" p-conflicts)
         (err-str "pid" "onyens" o-conflicts))))

(defn validate-student-picks! [valid-course-id? students]
  (doseq [{:keys [onyen picks]} students]
    (assert (every? valid-course-id? picks)
            (format "At least one of the picks (%s) for onyen %s was not a valid course identifier."
                    (str/join ", " picks) onyen))))

(defn validate-semesters-left [students]
  (let [invalid-semesters-left? #(or (< % 1) (> % 10))
        invalid-student? (comp invalid-semesters-left? :semesters-left)
        invalid-students (->> students
                              (filter invalid-student?)
                              (sort-by :onyen))]
    (if (empty? invalid-students)
      ""
      (str "WARNING: the following students had a semesters-left value < 1 or > 10 and will be skipped:\n- "
           (->> invalid-students
                (map #(format "onyen: %s (semesters left: %d)"
                              (:onyen %) (:semesters-left %)))
                (str/join "\n- "))
           "\n\n"))))

(defn validate-courses-needed [students]
  (let [invalid-courses-needed? #(< (:courses-needed %) 1)
        invalid-students (->> students
                              (filter invalid-courses-needed?)
                              (sort-by :onyen))]
    (if (empty? invalid-students)
      ""
      (str "WARNING: the following students had a courses-needed value < 1 and will be skipped:\n- "
           (->> invalid-students
                (map #(format "onyen: %s (courses needed: %d)"
                              (:onyen %) (:courses-needed %)))
                (str/join "\n- "))
           "\n\n"))))

(defn validate-output-noclobber! [output-dir output-file-base]
  (let [pattern (str output-dir "/" output-file-base "*.[ct]sv")
        matcher (.getPathMatcher (FileSystems/getDefault) (str "glob:" pattern))
        files (file-seq (io/file output-dir))
        matches? #(.matches matcher (.toPath %))
        matched (filterv matches? files)]
    (when (not-empty matched)
      (die! 1 (format (str "I was planning to write files to '%s',"
                           " but I found the following files that already exist,"
                           " so I'm cowardly refusing to proceed"
                           " to avoid overwriting output."
                           " Please delete these files or move them elsewhere,"
                           " or use a different output base (see -o option),"
                           " then try again.\n\n- %s")
                      pattern
                      (->> matched
                           (map #(.toPath %))
                           sort
                           (str/join "\n- ")))))))

(defn validate-remote-courses [students courses]
  (let [remote-course? (set (map :id (filter :remote? courses)))
        non-remote-course-ids (fn [student]
                                (filterv (complement remote-course?)
                                         (:picks student)))
        bad-students (keep (fn [student]
                               (let [ids (non-remote-course-ids student)]
                                 (when-not (empty? ids)
                                   [(:onyen student) ids])))
                           students)
        warnings (when-not (empty? bad-students)
                   (str "WARNING: the following remote students"
                        " picked non-remote courses:\n- "
                        (->> bad-students
                             (map (fn [[onyen ids]]
                                    (format "onyen: %s (non-remote courses: %s)"
                                            onyen
                                            (str/join ", " ids))))
                             (str/join "\n- "))
                        "\n\n"))
        students (vec
                   (for [student students]
                     (update student :picks #(filter remote-course? %))))]
    [students warnings]))
