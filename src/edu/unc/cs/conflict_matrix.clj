(ns edu.unc.cs.conflict-matrix
  (:require [clojure.java.io :as io]
            [clojure.string :as str]
            [clojure.tools.cli :as cli]
            [edu.unc.cs.course :refer [courses-conflict? new-course-spring22]]
            [edu.unc.cs.data-prep :refer [read-course-data]]
            [edu.unc.cs.util :refer [die! table->tsv]]))

(def cli-config
  (let [is-file? #(let [f (io/file %)] (and (.exists f) (.canRead f)))]
    [["-h" "--help"]
     ["-c" "--courses-csv FILE" "input CSV file for course data"
      :required "FILE"
      :validate [is-file? "Must be readable"]]]))

(def synopsis
  (format "conflict-matrix: %s\n\nUsage:\n    %s\n    %s    %s\n\n"
          "compute which classes conflict with which other classes"
          "clojure -M -m edu.unc.cs.conflict-matrix -c FILE"
          "java -cp edu.unc.cs.two-before-three.jar clojure.main \\\n"
          "      -m edu.unc.cs.conflict-matrix -c FILE"))

(defn validate-cli-args! [args]
  (let [opts (cli/parse-opts args cli-config)]
    (when (:help (:options opts))
      (die! 0 (str synopsis (:summary opts))))
    (let [{:keys [arguments errors options]} opts
          err-str "You must specify a CSV file with courses data (see -c option)"
          no-args (format "No extra arguments are accepted, but you gave: '%s'"
                          (str/join " " arguments))
          errors (cond-> (or errors [])
                   (-> options :courses-csv not) (conj err-str)
                   (pos? (count arguments)) (conj no-args))]
      (when (pos? (count errors))
        (die! 1
             (if (= 1 (count errors))
               (first errors)
               (str "There were some errors:\n\n- "
                    (str/join "\n- " errors)))))
      options)))

(def consv (comp vec cons))

(defn conflict-matrix [courses]
  (let [indexes (range (count courses))
        header-row (consv "course-id"
                          (map :id courses))]
    (consv header-row
           (for [i indexes
                 :let [ith-course (get courses i)]]
             (consv (get ith-course :id)
                    (for [j indexes
                          :let [jth-course (get courses j)]]
                      (if (courses-conflict? ith-course jth-course)
                        1
                        0)))))))

(defn -main [& args]
  (let [{:keys [courses-csv] :as config} (validate-cli-args! args)
        raw-courses (read-course-data courses-csv)
        courses (vec (sort-by :id (mapv new-course-spring22 raw-courses)))
        matrix (conflict-matrix courses)]
    (println (table->tsv matrix))))
