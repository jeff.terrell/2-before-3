(ns edu.unc.cs.util-test
  (:require [edu.unc.cs.util :as sut]
            [speclj.core :refer [describe it should= should-be
                                 should-have-invoked stub with with-stubs]]))

(describe "index"
  (with items [{:id 7} {:id 5}, {:id 3}])
  (with result (sut/index :id @items))
  (it "returns nil given an empty sequence"
    (should= nil (sut/index :id [])))
  (it "returns a hashmap"
    (should-be map? @result))
  (it "organizes data by key"
    (should= #{3 5 7} (-> @result keys set)))
  (it "does not lose any data"
    (should= (set @items) (-> @result vals set))))

(describe "spit-multiple"
  (with-stubs)
  (it "calls spit for each key/value pair in the given map"
    (with-redefs [spit (stub :spit)]
      (sut/spit-multiple {"file1" "contents1"
                          "file2" "contents2"})
      (should-have-invoked :spit {:times 2})
      (should-have-invoked :spit {:with ["file1" "contents1"]})
      (should-have-invoked :spit {:with ["file2" "contents2"]}))))

(describe "split-dir-base"
  (it "separates the directory part from the file part"
    (should= ["dir" "file"] (sut/split-dir-base "dir/file")))
  (it "includes multiple directories in the directory part if needed"
    (should= ["dir1/dir2" "file"] (sut/split-dir-base "dir1/dir2/file")))
  (it "respects a trailing slash to indicate a directory"
    (should= ["output" ""] (sut/split-dir-base "output/")))
  (it "supplies an implicit base dir of . if no explicit one given"
    (should= ["." "file"] (sut/split-dir-base "file"))))
