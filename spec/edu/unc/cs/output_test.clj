(ns edu.unc.cs.output-test
  (:require [edu.unc.cs.course :refer [->Course]]
            [edu.unc.cs.output :as sut]
            [edu.unc.cs.schedule :refer [parse-schedule]]
            [edu.unc.cs.student :refer [->Student]]
            [speclj.core :refer [context describe it should= should-be with]]))

(describe "student-results"
  (it "returns a sequence of student records"
    (let [result (sut/student-results
                   [{:id 123, :assignments ["onyen"]}]
                   [(->Student "123454321" "onyen" [123] 1 1 false)])]
      (should-be sequential? result)
      (should= edu.unc.cs.student.Student (type (first result)))))

  (it "adds an assignments vector to each student"
    (let [student (->Student "123454321" "onyen" [123] 1 1 false)
          result (sut/student-results [{:id 123, :assignments ["onyen"]}]
                                      [student])]
      (should= [123] (-> result first :assignments))))

  (it "adds an assignment-ranks vector to each student"
    (let [student (->Student "123454321" "onyen" [123] 1 1 false)
          result (sut/student-results [{:id 123, :assignments ["onyen"]}]
                                      [student])]
      (should= [1] (-> result first :assignment-ranks))))

  (it "adds an unsatisfied-count to each student"
    (let [student (->Student "123454321" "onyen" [123] 1 2 false)
          result (sut/student-results [{:id 123, :assignments ["onyen"]}]
                                      [student])]
      (should= 1 (-> result first :unsatisfied-count)))))


(describe "cohort-results"
  (with cohort-key :semesters-left)
  (it "returns 1 item for each cohort"
    (should= 3
             (count
               (sut/cohort-results
                 @cohort-key
                 (let [mk-student #(assoc {:courses-needed 2} @cohort-key %)]
                   (mapv mk-student [1 1 2 2 2 3 3 3 3 3]))))))

  (context "for each cohort"
    (with students
      [{@cohort-key 1, :courses-needed 2, :picks [7 8 9], :assignments [7 9], :assignment-ranks [1 3]}
       {@cohort-key 1, :courses-needed 2, :picks [7 8 9], :assignments [8  ], :assignment-ranks [2  ]}
       {@cohort-key 1, :courses-needed 2, :picks [7    ], :assignments [7  ], :assignment-ranks [1  ]}])

    (with result
      (first (sut/cohort-results @cohort-key @students)))

    (it "returns the cohort number"
      (should= (@cohort-key (first @students))
               (:cohort @result)))

    (it "returns the cohort size"
      (should= 3 (:n @result)))

    (it "returns the total number of seats needed"
      (should= 5 (:seats-needed @result)))

    (it "returns the total number of seats assigned"
      (should= 4 (:seats-assigned @result)))

    (it "returns the total number of first choices assigned"
      (should= 2 (:first-choices @result)))

    (it "returns the average rank of first assignments (when available)"
      (should= (float 4/3) (:avg-first-rank @result)))

    (it "returns the average rank of second assignments (when available)"
      (should= (float 3) (:avg-second-rank @result)))))

(describe "course->blockreg-table"
  (it "returns a vector of PIDs + 'UGRD', one for each assigned student"
    (should= [["123454321" "UGRD"]]
             (let [students-by-onyen {"onyen" {:onyen "onyen", :pid "123454321"}}
                   courses {:assignments ["onyen"]}]
               (sut/course->blockreg-table students-by-onyen courses)))))

(describe "blockreg-csvs"
  (it "returns reasonable CSV filenames as the keys"
    (should= "base.blockreg.comp585H.604.A.pozefsky.csv"
             (let [course {:raw-number "585H.604"
                           :instructor "Pozefsky"
                           :block-id "A"}]
               (-> (sut/blockreg-csvs "base." nil [course]) first key))))
  (it "returns CSV strings of assignment data as the values"
    (should= "123454321,UGRD\n543212345,UGRD\n"
             (let [course {:raw-number "585H.604"
                           :instructor "Pozefsky"
                           :assignments ["o1" "o2"]}
                   students [{:onyen "o1", :pid "123454321"}
                             {:onyen "o2", :pid "543212345"}]]
               (-> (sut/blockreg-csvs "base." students [course]) first val)))))

(describe "courses->csv"
  (it "produces correct CSV output"
    (should= (str \uffef "Name,ID,Instructor(s),Schedule,Enroll Cap,Block ID\n"
                  "426.001 - <snip>,42,Mayer-Patel,TTh 3:30pm-4:45pm,199.0,CS042\n")
             (sut/courses->csv [(->Course 42 "426.001" 426 "Mayer-Patel"
                                          (parse-schedule "TTh 3:30pm-4:45pm")
                                          200 "CS042" ["onyen1"] false "1")]))))
