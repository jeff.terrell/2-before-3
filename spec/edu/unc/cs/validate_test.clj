(ns edu.unc.cs.validate-test
  (:require [edu.unc.cs.student :refer [->Student]]
            [edu.unc.cs.validate :as sut]
            [speclj.core :refer [describe it should= should-be should-throw
                                 with]])
  (:import (java.io StringWriter)))

(describe "validate-student-pid-onyen-consistency"
  (with make-student #(->Student %1 %2 [5] 2 4 false))
  (with make-students #(mapv (fn [args] (apply @make-student args)) %))
  (it "returns non-empty warning text when an inconsistency is detected"
    (should-be not-empty (sut/validate-student-pid-onyen-consistency
                           (@make-students [["pid1" "onyen1"]
                                            ["pid1" "onyen2"]])))
    (should-be not-empty (sut/validate-student-pid-onyen-consistency
                           (@make-students [["pid1" "onyen1"]
                                            ["pid2" "onyen1"]]))))
  (it "returns empty warning text if no inconsistencies detected"
    (should-be empty? (sut/validate-student-pid-onyen-consistency
                        (@make-students [["pid1" "onyen1"]
                                         ["pid2" "onyen2"]])))))

(describe "validate-student-picks!"
  (it "throws an exception when a pick is invalid"
    (should-throw
      (sut/validate-student-picks! #{1} [{:onyen "onyen", :picks [1 2]}])))
  (it "returns nil when all picks are valid"
    (should= nil
             (sut/validate-student-picks! #{1 2}
                                          [{:onyen "onyen", :picks [2 1]}]))))

(describe "validate-semesters-left"
  (it "returns a non-empty warning string when a student has an invalid semesters-left value"
    (should-be not-empty (sut/validate-semesters-left
                           [(->Student "pid" "onyen" [] 0 2 false)]))
    (should-be not-empty (sut/validate-semesters-left
                           [(->Student "pid" "onyen" [] 11 2 false)])))
  (it "returns an empty warning string when all semesters-left values are OK"
    (should-be empty? (sut/validate-semesters-left
                        [(->Student "pid" "onyen" [] 4 2 false)]))))

(describe "validate-remote-courses"
  (it "returns nil warning text when only remote courses picked"
    (should= nil (second (sut/validate-remote-courses [] []))))
  (it "returns warning text when non-remote courses picked"
    (let [onyen "foo"
          student {:onyen onyen, :picks [7 8]}
          non-remote-course-id 7
          courses [{:id non-remote-course-id, :remote? false}
                   {:id                    8, :remote? true}]
          warning-str (second (sut/validate-remote-courses [student] courses))]
      (should= (format "WARNING: the following remote students picked non-remote courses:
- onyen: %s (non-remote courses: %d)\n\n"
                       onyen
                       non-remote-course-id)
               warning-str)))
  (it "filters non-remote picks from students"
    (let [student {:onyen "foo", :picks [7 8]}
          courses [{:id 7, :remote? false}
                   {:id 8, :remote? true}]]
      (should= [(assoc student :picks [8])]
               (first (sut/validate-remote-courses [student] courses))))))
