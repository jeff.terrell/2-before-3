(ns edu.unc.cs.course-test
  (:require [edu.unc.cs.course :as sut]
            [speclj.core :refer [context describe it should= should-be with]]))

(describe "courses-conflict?"
  (it "returns false when there is no conflict"
    (should=
      false
      (sut/courses-conflict?
        (sut/new-course ["585 ..." "1" "Pozefsky" "MW 1:25pm-2:40pm" "40" "A" "" "1"])
        (sut/new-course ["523 ..." "3" "Terrell" "TTh 1:25pm-2:40pm" "80" "B" "" "2"]))))

  (it "detects when the names are equal"
    (should=
      true
      (sut/courses-conflict?
        (sut/new-course ["585 ..." "1" "Pozefsky" "MW 1:25pm-2:40pm" "40" "A" "" "1"])
        (sut/new-course ["585H ..." "2" "Pozefsky" "Fr 1:25pm-2:40pm" "40" "B" "" "2"]))))

  (it "detects when the schedules conflict"
    (should=
      true
      (sut/courses-conflict?
        (sut/new-course ["585 ..." "1" "Pozefsky" "MW 1:25pm-2:40pm" "40" "A" "" "1"])
        (sut/new-course ["523 ..." "3" "Terrell" "MW 1:25pm-2:40pm" "80" "B" "" "2"])))))

(describe "make-courses-conflict?-fn"
  (with courses
    [(sut/new-course ["585 ..." "1" "Pozefsky" "MW 1:25pm-2:40pm" "40" "A" "" "1"])
     (sut/new-course ["585H .." "2" "Pozefsky" "Fr 1:25pm-2:40pm" "40" "B" "" "2"])
     (sut/new-course ["523 ..." "3" "Terrell" "TTh 1:25pm-2:40pm" "80" "C" "" "3"])
     (sut/new-course ["521 ..." "4" "Bishop"   "MW 1:25pm-2:40pm" "80" "D" "" "4"])])
  (with result (sut/make-courses-conflict?-fn @courses))

  (it "returns a function"
    (should-be fn? @result))

  (context "the returned function"
    ;; it is also faster than calling `courses-conflict?` every time, but that's
    ;; a non-functional requirement so we won't test that here.
    (it "returns correct conflict status given only course IDs"
      (should= true  (@result 1 1))
      (should= true  (@result 1 2))
      (should= false (@result 1 3))
      (should= true  (@result 1 4))
      (should= true  (@result 2 1))
      (should= true  (@result 2 2))
      (should= false (@result 2 3))
      (should= false (@result 2 4))
      (should= false (@result 3 1))
      (should= false (@result 3 2))
      (should= true  (@result 3 3))
      (should= false (@result 3 4))
      (should= true  (@result 4 1))
      (should= false (@result 4 2))
      (should= false (@result 4 3))
      (should= true  (@result 4 4)))))
