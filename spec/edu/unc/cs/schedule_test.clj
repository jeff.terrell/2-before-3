(ns edu.unc.cs.schedule-test
  (:require [edu.unc.cs.interval :refer [parse-interval]]
            [edu.unc.cs.schedule :as sut]
            [speclj.core :refer [describe it should-have-invoked should-throw
                                 should= stub with with-stubs]]))

(describe "parse-schedule"
  (with-stubs)
  (it "throws an exception with invalid input"
    (should-throw (sut/parse-schedule "abc123")))

  (it "parses the interval"
    (let [my-stub (stub :parse-interval)
          interval-str "the-interval-string"
          input (str "TTh " interval-str)]
      (with-redefs [parse-interval my-stub]
        (sut/parse-schedule input))
      (should-have-invoked :parse-interval {:times 1, :with [interval-str]})))

  (it "replicates the interval for each referenced weekday"
    (let [schedule :the-parsed-schedule
          my-stub (stub :parse-interval {:return schedule})
          input (str "TTh the-interval-string")]
      (with-redefs [parse-interval my-stub]
        (let [result (sut/parse-schedule input)]
          (should= (sut/map->Schedule {:input-str input
                                       :mon []
                                       :tue [schedule]
                                       :wed []
                                       :thu [schedule]
                                       :fri []})
                   result))))))

(describe "schedules-conflict?"
  (with check #(sut/schedules-conflict? (sut/parse-schedule %1)
                                        (sut/parse-schedule %2)))

  (it "detects all 4 types of overlaps between schedules"
    (should= true (@check "Mo 1:25pm-2:40pm" "Mo 2:39pm-3:55pm"))
    (should= true (@check "Tu 2:39pm-3:55pm" "Tu 1:25pm-2:40pm"))
    (should= true (@check "We 2:39pm-2:40pm" "We 1:25pm-3:55pm"))
    (should= true (@check "Th 1:25pm-3:55pm" "Th 2:39pm-2:40pm")))

  (it "respects the difference between weekdays"
    (should= false (@check "Tu 1:25pm-2:40pm" "We 1:25pm-2:40pm")))

  (it "does not care about abutments"
    (should= false (@check "Tu 1:25pm-2:40pm" "Tu 2:40pm-3:55pm"))))
