(ns edu.unc.cs.data-prep-test
  (:require [edu.unc.cs.data-prep :as sut]
            [edu.unc.cs.student :refer [->Student]]
            [speclj.core :refer [describe it should= should-be
                                 should-not-contain with]]))

(describe "clamp-courses-needed"
  (with input {:courses-needed 3})
  (with result (first (sut/clamp-courses-needed [@input])))
  (it "clamps the courses needed value to a max of 2"
    (should= 2
             (:courses-needed @result)))
  (it "adds a :submitted-courses-needed key to each student"
    (should= 3
             (:submitted-courses-needed @result))))

(describe "override-earlier-submissions"
  (it "passes all non-conflicting student records along"
    (should= 2 (count (sut/override-earlier-submissions
                        [(->Student "pid1" "onyen1" [5] 2 4 false)
                         (->Student "pid2" "onyen2" [5] 2 4 false)]))))
  (it "ignores the earlier picks when student records conflict"
    (let [earlier (->Student "pid1" "onyen1" [5] 2 4 false)
          later (->Student "pid1" "onyen1" [7] 2 4 false)
          result (sut/override-earlier-submissions [earlier later])]
      (should= [later] result))))

(describe "separate-cohort"
  (with student1 (->Student "pid1" "onyen1" [1 2 3] 1 2 false))
  (with student2 (->Student "pid2" "onyen2" [2 3 4] 2 4 false))
  (with students [@student1 @student2])
  (with cohort-key :semesters-left)
  (with result (sut/separate-cohort @cohort-key @students))

  (it "returns nil when no students remain"
    (should= nil (sut/separate-cohort @cohort-key [])))

  (it "indexes the returned cohort by onyen"
    (should-be map? (first @result))
    (should= [(:onyen @student1)] (keys (first @result))))

  (it "does not index the returned students"
    (should-be sequential? (second @result)))

  (it "separates students with the minimum @cohort-key value"
    (should= [1] (map @cohort-key (-> @result first vals)))
    (should-not-contain 1 (map @cohort-key (second @result))))

  (it "preserves the overall count"
    (should= (count @students) (apply + (map count @result)))))
