(ns edu.unc.cs.two-before-three-test
  (:require [edu.unc.cs.student :refer [->Student]]
            [edu.unc.cs.two-before-three :as sut]
            [speclj.core :refer [describe it should=]])
  (:import (java.io StringWriter)))

(describe "the functional core"
  (it "returns the correct answer (integration test)"
    (let [raw-courses
          ;; [name _ id _ _ _ instructor schedule-str block-id cap modality course-id]
          [["426.001 - Mayer-Patel TTh 3:30pm-4:45pm" nil "426" nil nil nil
            "Mayer-Patel" "TTh 3:30pm-4:45pm" "A" "10.0" "" "1"]
           ["431.001 - Kaur TTh 11am-12:15pm" nil "431" nil nil nil "Kaur"
            "TTh 11am-12:15pm" "B" "1.0" "" "2"]
           ["455.002 - Plaisted MW 1:25pm-2:40pm" nil "455" nil nil nil
            "Plaisted" "MW 1:25pm-2:40pm" "C" "1.0" "" "3"]
           ["520.001 - Prins TTh 12:30pm-1:45pm" nil "520" nil nil nil "Prins"
            "TTh 12:30pm-1:45pm" "D" "1.0" "" "4"]
           ["521.001 - Mayer-Patel TTh 9:30am-10:45am" nil "521" nil nil nil
            "Mayer-Patel" "TTh 9:30am-10:45am" "E" "1.0" "" "5"]]

          s1-pid "123123123"
          s2-pid "234234234"
          j-pid "345345345"
          so-pid "456456456"

          raw-students
          [;; senior1 wants 1 course and gets 1 although could get 2 (431 426)
           [s1-pid "" "" "asmith" "431, 426" "" "1" "1" "" "" ""]
           ;; same onyen, different pid; this row should get overridden
           [(str s2-pid "0") "" "" "bsmith" "426, 431" "" "1" "2" "" "" ""]
           ;; senior2 wants 2 courses but only gets 1 (426 431)
           [s2-pid "" "" "bsmith" "426, 431" "" "1" "2" "" "" ""]
           ;; junior wants 4 courses but only gets 2 (426 455 / 520 521)
           [j-pid "" "" "csmith" "426, 455, 520, 521" "" "3" "4" "" "" ""]
           ;; sophomore student doesn't get any picks (431 455)
           [so-pid "" "" "dsmith" "431, 455" "" "5" "6" "" "" ""]]

          ;; one course does not fill (426)
          a426 (format "%s,UGRD\n%s,UGRD\n" s2-pid j-pid)
          ;; one course fills and doesn't assign any more students even though more want it (431)
          a431 (format "%s,UGRD\n" s1-pid)
          a455 (format "%s,UGRD\n" j-pid)
          ;; two courses have no assignments
          a520 ""
          a521 ""

          human-header (str "PID\tonyen\tsemesters-left\tcourses-needed\tpicks\t"
                            "assignments\tassignment-ranks\tunsatisfied-count\n")
          ;; warning: brittle coupling in this block of code
          s1-result "123123123\tasmith\t1\t1\t\"431,426\"\t\"431\"\t\"1\"\t0\n"
          s2-result "234234234\tbsmith\t1\t2\t\"426,431\"\t\"426\"\t\"1\"\t1\n"
          j-result "345345345\tcsmith\t3\t4\t\"426,455,520,521\"\t\"426,455\"\t\"1,2\"\t0\n"
          so-result "456456456\tdsmith\t5\t6\t\"431,455\"\t\"\"\t\"\"\t2\n"

          h426 (str human-header s2-result j-result)
          h431 (str human-header s1-result)
          h455 (str human-header j-result)
          h520 human-header
          h521 human-header

          results-tsv (str "PID\tonyen\tsemesters-left\tcourses-needed\tpicks"
                           "\tassignments\tassignment-ranks\tunsatisfied-count\n"
                           so-result s2-result s1-result j-result)

          warnings-txt (str "WARNING: different pids were seen for the same"
                            " onyen in the following cases:\n"
                            "- onyen: 'bsmith'; pids: 2342342340, 234234234"
                            " (using last value 234234234)\n\n")

          cohort-tsv
          (str "cohort\tN\tseats-needed\tseats-assigned\tfirst-choices\tavg-first-rank\tavg-second-rank\tavg-picklist-length\n"
               "1\t2\t3\t2\t2\t1.0\t\t2.0\n"
               "3\t1\t2\t2\t1\t1.0\t2.0\t4.0\n"
               "5\t1\t2\t0\t0\t\t\t2.0\n")

          courses-csv
          (str \uffef "Name,ID,Instructor(s),Schedule,Enroll Cap,Block ID\n"
               "426.001 - <snip>,426,Mayer-Patel,TTh 3:30pm-4:45pm,8.0,A\n"
               "431.001 - <snip>,431,Kaur,TTh 11am-12:15pm,0.0,B\n"
               "455.002 - <snip>,455,Plaisted,MW 1:25pm-2:40pm,0.0,C\n"
               "520.001 - <snip>,520,Prins,TTh 12:30pm-1:45pm,1.0,D\n"
               "521.001 - <snip>,521,Mayer-Patel,TTh 9:30am-10:45am,1.0,E\n")

          block-enrollment-cover-sheet-csv
          (str \uffef "COMP COURSE,Student Block,Class Block,Class Description,Total Students,Course Number,Requistes Override,Instructor Permission,RECITATION Section Number,Total Course Enrollment Capacity\n"
               "COMP 426-001,CS426,CS426,CS426.A,2,1,Y,Y,N/A,10\n"
               "COMP 431-001,CS431,CS431,CS431.B,1,2,Y,Y,N/A,1\n"
               "COMP 455-002,CS455,CS455,CS455.C,1,3,Y,Y,N/A,1\n"
               "COMP 520-001,CS520,CS520,CS520.D,0,4,Y,Y,N/A,1\n"
               "COMP 521-001,CS521,CS521,CS521.E,0,5,Y,Y,N/A,1\n")

          requests-csv
          (str \uffef "PID,First Name,Last Name,ONYEN,Course Selections,Email,"
               "Semesters Remaining,Courses Needed,"
               "Computer Science Degree Program,Created By\n"
               "234234234,,,bsmith,431,,1,1,,\n"
               "456456456,,,dsmith,\"431, 455\",,5,2,,\n")

          course-stats-tsv
          (str "number.section\tinstructor\tremote?\tcap\tnum-picks\tavg-pick-rank"
               "\tnum-assigned\tremaining-space\tassigned-avg-cohort\n"
               "426.001\tMayer-Patel\tno\t10\t3\t1.3333334\t2\t8\t2.0\n"
               "431.001\tKaur\tno\t1\t3\t1.3333334\t1\t0\t1.0\n"
               "455.002\tPlaisted\tno\t1\t2\t2.0\t1\t0\t3.0\n"
               "520.001\tPrins\tno\t1\t1\t3.0\t0\t1\t\n"
               "521.001\tMayer-Patel\tno\t1\t1\t4.0\t0\t1\t\n")

          result (let [s (new StringWriter)]
                   (binding [*err* s]
                     (sut/functional-core {:output-base "s21."
                                           :cohort-key :semesters-left
                                           :give-complete-students-1-course true}
                                          [raw-courses raw-students])))]

      (should= (sorted-set "s21.blockreg.comp426.001.A.mayer-patel.csv"
                           "s21.blockreg.comp431.001.B.kaur.csv"
                           "s21.blockreg.comp455.002.C.plaisted.csv"
                           "s21.blockreg.comp520.001.D.prins.csv"
                           "s21.blockreg.comp521.001.E.mayer-patel.csv"
                           "s21.blockreg.cover-sheet.csv"
                           "s21.human.comp426.001.A.mayer-patel.tsv"
                           "s21.human.comp431.001.B.kaur.tsv"
                           "s21.human.comp455.002.C.plaisted.tsv"
                           "s21.human.comp520.001.D.prins.tsv"
                           "s21.human.comp521.001.E.mayer-patel.tsv"
                           "s21.warnings.txt"
                           "s21.student-results.tsv"
                           "s21.cohort-results.tsv"
                           "s21.course-stats.tsv"
                           "s21.courses-out.csv"
                           "s21.requests-out.csv")
               (->> result keys (apply sorted-set)))

      (should= a426 (result "s21.blockreg.comp426.001.A.mayer-patel.csv"))
      (should= a431 (result "s21.blockreg.comp431.001.B.kaur.csv"))
      (should= a455 (result "s21.blockreg.comp455.002.C.plaisted.csv"))
      (should= a520 (result "s21.blockreg.comp520.001.D.prins.csv"))
      (should= a521 (result "s21.blockreg.comp521.001.E.mayer-patel.csv"))
      (should= h426 (result "s21.human.comp426.001.A.mayer-patel.tsv"))
      (should= h431 (result "s21.human.comp431.001.B.kaur.tsv"))
      (should= h455 (result "s21.human.comp455.002.C.plaisted.tsv"))
      (should= h520 (result "s21.human.comp520.001.D.prins.tsv"))
      (should= h521 (result "s21.human.comp521.001.E.mayer-patel.tsv"))
      (should= warnings-txt (result "s21.warnings.txt"))
      (should= results-tsv (result "s21.student-results.tsv"))
      (should= cohort-tsv (result "s21.cohort-results.tsv"))
      (should= course-stats-tsv (result "s21.course-stats.tsv"))
      (should= courses-csv (result "s21.courses-out.csv"))
      (should= requests-csv (result "s21.requests-out.csv"))
      (should= block-enrollment-cover-sheet-csv (result "s21.blockreg.cover-sheet.csv")))))
