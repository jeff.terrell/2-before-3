(ns edu.unc.cs.assign-test
  (:require
   [edu.unc.cs.assign :as sut]
   [edu.unc.cs.course :refer [->Course]]
   [edu.unc.cs.data-prep :as dp]
   [edu.unc.cs.schedule :refer [parse-schedule]]
   [edu.unc.cs.student :refer [->Student]]
   [edu.unc.cs.util :refer [index]]
   [speclj.core :refer [around context describe it should= should-have-invoked
                        should-not-contain should-not-have-invoked stub with
                        with-stubs]]))

(describe "assign"
  (with assignments ["onyen1" "onyen2"])
  (with onyen "onyen3")
  (with course-id 123)
  (with schedule [(parse-schedule "MW 12:30pm-1:00pm")])
  (with course
    (->Course @course-id nil 426 "instructor" @schedule 100 "A" @assignments
              false "1"))
  (with conflicting-id 321)
  (with conflicting-course
    (->Course @conflicting-id nil 590 "jones" @schedule 8 "B" [] false "2"))
  (with student
    (->Student "pid" @onyen [@course-id @conflicting-id 77] 4 2 false))

  (with data
    {:courses (index :id [@course @conflicting-course])
     :current-cohort {@onyen @student}
     :courses-conflict? (fn [id1 id2]
                          ;; id1 is always @course-id in these tests
                          (#{@course-id @conflicting-id} id2))})
  (with result (sut/assign @data @course-id @onyen))

  (it "adds the onyen to the vector of assignments for the course"
    (should= (conj @assignments @onyen)
             (get-in @result [:courses @course-id :assignments])))

  (it "decrements the courses-needed number for the student"
    (should= (dec (:courses-needed @student))
             (get-in @result [:current-cohort @onyen :courses-needed])))

  (it "removes the course from the list of picks for the student"
    (should-not-contain @course-id
                        (get-in @result [:current-cohort @onyen :picks])))

  (it "removes conflicting courses from the list of picks for the student"
    (should-not-contain @conflicting-id
                        (get-in @result [:current-cohort @onyen :picks]))))

(describe "assign-multiple"
  (with-stubs)
  (with course-id :a-course-id)
  (with data {:courses {@course-id {:assignments [], :cap 2}}})
  (with intermediate-result :an-intermediate-result)
  (with shuffled [:onyen3 :onyen2 :onyen1])
  (around [it]
    (with-redefs [sut/assign (stub :assign {:return @intermediate-result})
                  shuffle (stub :shuffle {:return @shuffled})]
      (it)))

  (it "calls assign for each student if there's room for all the students"
    (let [onyens [:onyen1 :onyen2]]
      (sut/assign-multiple @data @course-id onyens)
      (should-have-invoked :assign {:times 2})
      (should-have-invoked :assign {:with [@data @course-id :onyen1]})
      (should-have-invoked :assign {:with [:* @course-id :onyen2]})))

  (it "accumulates the results from successive calls to assign"
    (let [onyens [:onyen1 :onyen2]]
      (sut/assign-multiple @data @course-id onyens)
      (should-have-invoked :assign
                           {:with [@intermediate-result @course-id :onyen2]})))

  (it "stops assigning students before the class overflows"
    (let [onyens [:onyen1 :onyen2 :onyen3]]
      (sut/assign-multiple @data @course-id onyens)
      (should-have-invoked :assign {:times 2})))

  (it "randomizes who gets a seat in the overflow case"
    (let [onyens [:onyen1 :onyen2 :onyen3]]
      (sut/assign-multiple @data @course-id onyens)
      (should-have-invoked :shuffle {:times 1, :with [onyens]})
      (should-have-invoked :assign {:times 2})
      (should-have-invoked :assign {:with [@data @course-id :onyen3]})
      (should-have-invoked :assign {:with [:* @course-id :onyen2]}))))

(describe "sweep-course"
  (it "removes the course from the index of courses"
    (let [data {:courses {1 {:id 1}}
                :current-cohort []
                :students []}
          result (sut/sweep-course data 1)]
      (should= {} (:courses result))))

  (it "adds the course (including all data) to the :full-courses seq"
    (let [course (->Course 1 nil 426 "KMP" "MW 12:30pm-1:45pm" 300 "A" [] false "3")
          data {:courses {1 course}
                :current-cohort []
                :students []}
          result (sut/sweep-course data 1)]
      (should= [course] (:full-courses result))))

  (it "removes the course from picks in the current cohort"
    (let [data {:current-cohort {"o1" {:picks [1 2 3]}
                                 "o2" {:picks [2 3 1]}
                                 "o3" {:picks [3 1 2]}}
                :students []}
          result (sut/sweep-course data 1)]
      (should= {"o1" {:picks [2 3]}
                "o2" {:picks [2 3]}
                "o3" {:picks [3 2]}}
               (:current-cohort result))))

  (it "removes the course from picks in the students not in the current cohort"
    (let [data {:current-cohort []
                :students [{:picks [1 2 3]}
                                     {:picks [2 3 1]}
                                     {:picks [3 1 2]}]}
          result (sut/sweep-course data 1)]
      (should= [{:picks [2 3]}
                {:picks [2 3]}
                {:picks [3 2]}]
               (:students result)))))

(describe "assign-multiple-and-sweep-if-needed"
  (with-stubs)
  (with course-id 1234)
  (with data {})
  (with onyens [:onyen1])
  (with updated-data
    (fn [cap]
      {:courses {@course-id {:cap cap, :assignments @onyens}}}))
  (with run
    (fn [cap]
      (with-redefs [sut/assign-multiple (stub :assign-multiple
                                              {:return (@updated-data cap)})
                    sut/sweep-course (stub :sweep-course-from-picks)]
        (sut/assign-multiple-and-sweep-if-needed @data @course-id @onyens))))

  (it "calls assign-multiple"
    (@run 1)
    (should-have-invoked :assign-multiple
                         {:times 1, :with [@data @course-id @onyens]}))

  (it "calls sweep if the class became full after the assignments"
    (@run 1)
    (should-have-invoked :sweep-course-from-picks
                         {:times 1, :with [(@updated-data 1) @course-id]}))

  (it "does not call sweep if the class is still not full"
    (@run 2)
    (should-not-have-invoked :sweep-course-from-picks)))

(describe "assign-top-picks"
  (with-stubs)
  (it "calls assign-multiple-and... with each group of students sharing a top pick"
    (let [data {:current-cohort {"onyen1" {:onyen "onyen1", :picks [1 2 3]}
                                 "onyen2" {:onyen "onyen2", :picks [2 1]}
                                 "onyen3" {:onyen "onyen3", :picks [1 3]}}}]
      (with-redefs [sut/assign-multiple-and-sweep-if-needed
                    (stub :assign-multiple-and-sweep-if-needed)]
        (sut/assign-top-picks data))
      (should-have-invoked :assign-multiple-and-sweep-if-needed {:times 2})
      (should-have-invoked :assign-multiple-and-sweep-if-needed
                           {:with [:* 1 ["onyen1" "onyen3"]]})
      (should-have-invoked :assign-multiple-and-sweep-if-needed
                           {:with [:* 2 ["onyen2"]]})))

  (it "accumulates the results from assign-multiple-and..."
    (let [data {:current-cohort {"onyen1" {:onyen "onyen1", :picks [1 2 3]}
                                 "onyen2" {:onyen "onyen2", :picks [2 1]}
                                 "onyen3" {:onyen "onyen3", :picks [1 3]}}}]
      (with-redefs [sut/assign-multiple-and-sweep-if-needed
                    (stub :assign-multiple-and-sweep-if-needed
                          {:return :intermediate-result})]
        (sut/assign-top-picks data))
      (should-have-invoked :assign-multiple-and-sweep-if-needed {:times 2})
      (should-have-invoked :assign-multiple-and-sweep-if-needed
                           {:with [data 1 ["onyen1" "onyen3"]]})
      (should-have-invoked :assign-multiple-and-sweep-if-needed
                           {:with [:intermediate-result 2 ["onyen2"]]}))))

(describe "sweep-students"
  (it "removes students who have no more courses needed"
    (let [data {:current-cohort {"o" {:onyen "o"
                                      :courses-needed 0
                                      :picks [1]}}}]
      (should= 0 (-> data sut/sweep-students :current-cohort count))))

  (it "removes students who have no picks remaining"
    (let [data {:current-cohort {"o" {:onyen "o"
                                      :courses-needed 1
                                      :picks []}}}]
      (should= 0 (-> data sut/sweep-students :current-cohort count))))

  (it "keeps students who have courses needed and picks remaining"
    (let [data {:current-cohort {"o" {:onyen "o"
                                      :courses-needed 1
                                      :picks [1]}}}]
      (should= 1 (-> data sut/sweep-students :current-cohort count))))

  (it "affects both :current-cohort and :students"
    (let [data {:current-cohort {"o" {:onyen "o"
                                      :courses-needed 1
                                      :picks []}}
                :students [{:courses-needed 1, :picks []}]}
          result (sut/sweep-students data)]
      (should= 0 (-> result :current-cohort count))
      (should= 0 (-> result :students count)))))

(describe "assign-current-cohort"
  (it "stops when no satisfiable students remain"
    (let [data {:courses {1 {:id 1, :assignments [], :cap 5}}
                :current-cohort {}}]
      (should= data (sut/assign-current-cohort data))))

  (it "stops when no courses with space remain"
    (let [data {:courses {1 {:id 1, :assignments ["onyen"], :cap 1}}
                :current-cohort {"o" {:onyen "o"
                                      :courses-needed 1
                                      :picks [1]}}}]
      (should= data (sut/assign-current-cohort data))))

  (it "calls assign-top-picks and sweep-students at each step"
    (let [data {:courses {1 {:id 1, :assignments ["onyen1"], :cap 5}}
                :courses-conflict? (constantly false)
                :current-cohort {"onyen2" {:onyen "onyen2"
                                           :courses-needed 1
                                           :picks [1]}}}]
      (should= {:courses {1 {:id 1, :assignments ["onyen1" "onyen2"], :cap 5}}
                :current-cohort {}
                :students []}
               (dissoc (sut/assign-current-cohort data) :courses-conflict?)))))

(describe "assign-all"
  (with-stubs)
  (with cohort-key :semesters-left)
  (context "when no cohort is available (i.e. no students left)"
    (around [it]
      (with-redefs [sut/assign-current-cohort (stub :assign-current-cohort)]
        (it)))

    (it "does not call assign-current-cohort"
      (sut/assign-all @cohort-key {})
      (should-not-have-invoked :assign-current-cohort))

    (it "returns the seq of full and not-full courses"
      (should= [:course1 :course2]
               (sut/assign-all @cohort-key
                               {:full-courses [:course1]
                                :courses {:id2 :course2}}))))

  (context "when a cohort is available"
    (around [it]
      (with-redefs [sut/assign-current-cohort (stub
                                                :assign-current-cohort
                                                {:return {:students
                                                          :intermediate-value}})
                    dp/separate-cohort (stub
                                         :separate-cohort
                                         {:invoke
                                          (let [next-return (atom [{"o" {:onyen "o"
                                                                         :courses-needed 1
                                                                         :picks [1]}}
                                                                   nil])]
                                            (fn [& _]
                                              (let [ret-val @next-return]
                                                (reset! next-return nil)
                                                ret-val)))})]
        (it)))

    (it "calls assign-current-cohort with cohort-separated data"
      (sut/assign-all @cohort-key
                      {:courses {1 :course1}
                       :students {"onyen" {:onyen "onyen"
                                           :courses-needed 1
                                           :picks [1]}}})
      (should-have-invoked :assign-current-cohort {:times 1}))

    (it "iterates based on returned value from assign-current-cohort"
      (sut/assign-all @cohort-key
                      {:courses {1 :course1}
                       :students {"onyen" {:onyen "onyen"
                                           :courses-needed 1
                                           :picks [1]}}})
      (should-have-invoked :separate-cohort {:times 2})
      (should-have-invoked :separate-cohort
                           {:with [@cohort-key :intermediate-value]}))))
