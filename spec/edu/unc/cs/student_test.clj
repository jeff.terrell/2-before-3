(ns edu.unc.cs.student-test
  (:require [edu.unc.cs.student :as sut]
            [speclj.core :refer [describe it should= should-throw]]))

(describe "new-student"
  (it "returns a Student record"
    (should= edu.unc.cs.student.Student
             (type (sut/new-student [:pid nil nil "onyen" "5" nil "2" "4" ""]))))
  (it "parses integer fields"
    (let [result (sut/new-student [:pid nil nil "onyen" "5" nil "2" "4" ""])]
      (should= 2 (:semesters-left result))
      (should= 4 (:courses-needed result))))
  (it "checks for invalid picks input"
    (should-throw AssertionError
                  (sut/new-student
                    [:pid nil nil "onyen" "5 7 9" nil "2" "4" ""])))
  (it "parses picks into a vector of ints"
    (should= [5 9 7]
             (:picks (sut/new-student
                       [:pid nil nil "onyen" "5, 9, 7" nil "2" "4" ""]))))
  (it "detects remote students"
    (should= true
             (:remote? (sut/new-student
                         [:pid nil nil "onyen" "5" nil "2" "4" "Remote"]))))
  (it "detects non-remote students"
    (should= false
             (:remote? (sut/new-student
                         [:pid nil nil "onyen" "5" nil "2" "4" "On Campus"])))))

