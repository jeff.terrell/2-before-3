(ns edu.unc.cs.interval-test
  (:require [edu.unc.cs.interval :as sut]
            [speclj.core :refer [describe it should-be should-throw should=]]))

(describe "parse-interval"
  (it "throws an exception if input is malformed"
    (should-throw (sut/parse-interval "a-b"))
    (should-throw (sut/parse-interval nil)))

  (it "returns a map of two LocalTime objects"
    (let [result (sut/parse-interval "11:30am-2:30pm")]
      (should-be map? result)
      (should= 2 (count result))
      (should= #{:begin :end} (-> result keys set))
      (should= java.time.LocalTime (type (:begin result)))
      (should= java.time.LocalTime (type (:end result)))))

  (it "tolerates abbreviation for on-the-hour times"
    (let [result (sut/parse-interval "2pm-2:50pm")]
      (should= 2 (count result)))))

(describe "intervals-conflict?"
  (it "detects all 4 types of overlaps between intervals"
    (should= true (sut/intervals-conflict?
                    (sut/parse-interval "1:25pm-2:40pm")
                    (sut/parse-interval "2:39pm-3:55pm")))
    (should= true (sut/intervals-conflict?
                    (sut/parse-interval "2:39pm-3:55pm")
                    (sut/parse-interval "1:25pm-2:40pm")))
    (should= true (sut/intervals-conflict?
                    (sut/parse-interval "1:25pm-3:55pm")
                    (sut/parse-interval "2:39pm-2:40pm")))
    (should= true (sut/intervals-conflict?
                    (sut/parse-interval "2:39pm-2:40pm")
                    (sut/parse-interval "1:25pm-3:55pm"))))

  (it "does not care about abutments"
    (should= false (sut/intervals-conflict?
                     (sut/parse-interval "1:25pm-2:40pm")
                     (sut/parse-interval "2:40pm-3:55pm")))))
