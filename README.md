# two before three

The reference implementation for the course preregistration system for UNC CS
majors.

The name "two before three" refers to the fact that any student who still needs
classes to make progress toward their degree is assigned up to two seats in
COMP courses before any student is assigned three seats in COMP courses.

## Synopsis

    clojure -M -m edu.unc.cs.two-before-three -s 20212 -c courses.csv -r requests.csv

...or, if you have an uberjar that you or a friend compiled with
`clojure -X:uberjar`, and you have a `java -version` greater than or equal to
the version used to compile the uberjar, you can bypass the Clojure CLI and run
the JAR directly like so:

    java -jar edu.unc.cs.two-before-three.jar -s 20212 -c courses.csv -r requests.csv

## Prerequisites

[Clojure CLI](https://clojure.org/guides/getting_started#_clojure_installer_and_cli_tools)

## Compatibility

Note: this program has not been tested on Windows. In particular, you may hit
issues when using an output base (-o option) with directories in it. However,
using WSL should work flawlessly.

## Inputs

There are two required CSV inputs and two optional settings specified on the
command line.

The optional settings are:

- `-o` or `--output-base`: the path that will be prepended to each output file.
  This value may contain slashes to indicate directories, which will be
  automatically created if they don't already exist. The default value is
  `output/`. If `.csv` or `.tsv` files already exist with this prefix, the
  program will exit to avoid overwriting existing outputs.
- `-s` or `--random-seed`: to enable deterministic results, you may specify a
  random seed for the pseudo-random number generator. If you don't, the seed
  will be arbitrary, and the results will (most likely) be different for every
  run, even for the same inputs.

You may also specify `-h` or `--help` to see the usage screen.

The required inputs are both in CSV form. The files are expected to contain a
[byte order mark](https://en.wikipedia.org/wiki/Byte_order_mark), which is
ignored. In actual usage, these files are downloaded from an Airtable base. Note
that the student-provided values for courses-needed and semesters-left are
verified against Connect Carolina data, to prevent the possibility of students
gaming the system to get higher priority for the classes they want.

- `-c` or `--courses-csv` is the path to a CSV file of course data with the
  following columns:
    - Name, including the course number, section, and other (redundant) info.
      The required parts at the beginning of the field are course number, a
      period, section number, and a space character. Everything else is ignored.
      For example: `426.001 - <snip>`.
    - ID: a numeric ID, used in student requests to indicate course selections
    - Instructor(s): a string, typically the instructor's last name
    - Schedule: a string in a certain format; see below for more details
    - Enroll Cap: a positive number ending in `.0`, e.g. `80.0`
    - Block ID: a unique string to be used during block enrollment, e.g. `CS001`
- `-r` or `--requests-csv` is the path to a CSV file of student request data
  with the following columns:
    - PID: the student's PID
    - First Name, ignored
    - Last Name, ignored
    - ONYEN, the student's onyen identifier
    - Course Selections, a comma+space-separated list of course IDs,
      e.g. `54, 66, 63`. Also known as the picklist.
    - Email, ignored
    - Semesters Remaining, a number, should be between 1 and 10 inclusive
    - Courses Needed, a number, should be positive
    - Computer Science Degree Program, BS or BA; ignored
    - Created By, ignored

The schedule field is complex and needs to be parsed by the program to
automatically determine time-based conflicts between classes, so that if a
student is assigned a course, time-conflicting courses also in the student's
picklist are removed. So the details matter. Rather than specify a full grammar,
here are a few example strings that are valid.

- `TTh 3:30pm-4:45pm`
- `MWF 11:15am-12:05pm`
- `TTh 5pm-6:15pm`
- `TTh 12:30pm-1:45pm (Rec: We 9:05am-9:55am)` (Note in this case that the
  weekday is always two letters and that either time can cause the entire course
  to conflict with another one.)

## Warnings

The student request data is specified by students. It is not pre-validated and
might contain various inconsistencies. Such issues are noted in the output of
the program but do not cause a fatal error. Here is a list of such issues and
their automatic resolutions:

- more than one request seen for the same PID/onyen: use the latest value
- different PIDs seen for the same onyen: use the latest value
- different onyens seen for the same PID: use the latest value
- semesters-left value is < 1 or > 10: skip that request
- courses-needed < 1: skip that request

## Outputs

Various outputs are created by successful operation. Assuming an output base (-o
switch) of `output/` (the default):

- Any printed warnings are also saved to `output/warnings.txt`.
- Block registration CSVs are at
  `output/blockreg.{course}.{section}.{block-id}.{instructor}.csv`. These files,
  one per course, are used by the student services manager in Connect Carolina
  to actually register students for classes.
- Human-readable per-course CSVs, with parallel filenames and student identities
  as the block registration CSVs, are at
  `output/human.{course}.{section}.{block-id}.{instructor}.tsv`. These contain
  more info than the block registration CSVs so are easier for humans to use.
- Data about student results is at `output/student-results.tsv`. This file lists
  assignments and various info for each student, including the ranks of
  assignments within the students picklist, and how many courses they wanted
  (max 2) but didn't get assigned (called the unsatisfied count).
- Analysis about cohorts is at `output/cohort-results.tsv`. This file computes
  various statistics for each cohort. (See the algorithm details section to
  understand what a cohort is.) More details about this attachment follow.
- Analysis about courses is at `output/course-stats.tsv`. This file computes
  various statistics for each course, e.g. the average number of semesters left
  of the students assigned to the course. More details about this attachment
  follow.
- Final course data is at `output/courses-out.csv`. This file is like the
  `courses.csv` input (and indeed can be used as that input in a future run),
  but is updated to reduce the cap of each course by the number of seats already
  assigned.
- Final student request data is at `output/requests-out.csv`. This file is like
  the `requests.csv` input (and indeed can be used as that input in a futere
  run), but is updated to reflect the assignments already made for that student.

### cohort-results.tsv

The `cohort-results.tsv` output computes various statistics about each cohort,
which by default is a group of students sharing a single semesters-left value.
The columns are:

1. `cohort`: which cohort is this, i.e. what is the shared semesters-left value?
2. `N`: how many students are in this cohort?
3. `seats-needed`: the sum of all the courses needed values in the cohort, where
   each student's value can be at most 2
4. `seats-assigned`: the number of seats actually assigned by the algorithm
5. `first-choices`: how many of the assignments were for the student's top pick?
6. `avg-first-rank`: among students who got one assignment, what is the rank of
   that assignment within their picklist? Average all ranks.
7. `avg-second-rank`: among students who got two assignments, what is the rank
   of the second assignment within their picklist? Average all ranks.
8. `avg-picklist-length`: how long is each student's picklist? Average all
   lengths.

### course-stats.tsv

The `course-stats.tsv` output computes various statistics about each course. The
columns are:

1. `number.section`, e.g. 426.001 for Ketan's COMP 426 class
2. `instructor`
3. `cap`, the enrollment cap
4. `num-picks`: the number of students who included this course in their
   picklist
5. `avg-pick-rank`: among students who picked this course, what rank is the
   class within their picklist? Average all ranks.
6. `num-assigned`: how many students were actually assigned to this course by
   the algorithm?
7. `remaining-space`: the difference between the cap and the number assigned.
8. `assigned-avg-cohort`: among students assigned to this course, which cohort
   did the student belong to? Average all cohorts.

## Algorithm details

Before the algorithm begins, various validation checks are performed, including
those listed in the Warnings section above.

First, the student requests are grouped by cohort. A cohort is the set of
students sharing a particular semesters-left value. The cohorts are sorted so
that the most senior (i.e. with the smallest semesters-left value) is first.

Then assignments are made for each cohort as follows. The top picks of the
students are collected and grouped by course. For each such group, the size of
the group is compared to the remaining room in the course. If the group is small
enough to fit without exceeding the enrollment cap, then all students in the
group are assigned to the course. Otherwise, the students in the group are
randomly shuffled and students are assigned to the course until it is full.
(Note: this is the only use of randomness in the program.)

When a student is assigned to the course, a few things happen. The course is
updated to note the onyen of the assigned student. The student is updated to
reduce the number of courses they need by one. The student's picklist is updated
to remove any classes that conflict with the assigned class (which includes the
assigned class itself).

If a class became full when assigning a group of students to it, that class is
removed from the picklists of all students.

After the top picks are assigned, students who either have no remaining courses
in their picklist or have a courses-needed value of 0 are removed from the
current cohort.

If there are still classes that aren't full and students in the current cohort
who need classes, continue to group students in the current cohort by their top
pick. Otherwise, move on to the next cohort.

When no cohorts remain, all assignments are made, and the output phase begins.

### Algorithm invariants

- all students in the current cohort still need at least 1 course
- all students in the current cohort still have non-full courses in their
  picklists

### Algorithm properties

- Students who submit a shorter picklist have no advantage over those who submit
  a longer picklist; indeed, they are more likely to fail to be assigned as many
  courses as they need.
- Students in earlier cohorts get their 2nd pick before students in later
  cohorts get their first pick.

Jack Snoeyink suggested that reversing the 2nd property might result in a better
overall fit, at least by some metrics. This hypothesis can be investigated by
calling the program more than once and massaging the inputs at each step, as
follows. First, cap all student requests to have only 1 needed course (and note
which students actually need 2). Then call the program. Then modify the
`requests-out.csv` file to add 1 to the courses-needed value for students who
originally needed 2 courses, and call the program again with the modified
requests file and the `courses-out.csv` file from the first run.

## Tests

Unit tests, and an integration test or two, help me have confidence that my
program is correct. The tests are written in a behavior-oriented style, similar
to Jest or RSpec.

Before running the tests, you'll need to do the following one-time setup:

```
mkdir classes
clojure -M:speclj -e "(do (compile 'speclj.platform.SpecFailure)
                          (compile 'speclj.platform.SpecPending))"
```

To run the tests, say:

    clojure -M:test

To start a test auto-runner, which will watch your source files for changes and
re-run the tests that might have been affected (hooray for explicit imports!),
say:

    clojure -M:test -a

## See the course conflict matrix

To generate a course conflict matrix in TSV format, run:

    clojure -M -m edu.unc.cs.conflict-matrix -c COURSES_TSV

Or, if you've generated an uberjar already, you can also run:

    java -cp edu.unc.cs.two-before-three.jar clojure.main \
          -m edu.unc.cs.conflict-matrix -c COURSES_TSV

## License

Copyright © 2021 Jeff Terrell

Distributed under the MIT License.
